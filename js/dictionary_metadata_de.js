var dictionary_metadata_de=(function (){
	var elements=[{"type":"field_input","id":"creator","tag":"creator","tag_type":"input","label":"Author","placehoder":"Name as Input","validation_msg":"Pflichtfeld","help":"Name der Person, die für den Inhalt verantwortlich sind und über das Urheberrecht verfügen","additional_data":[]},
{"type":"add_button","id":"co_creators","tag":"co_creator","multiple_class":"creator"},
{"type":"dropdown_normal","id":"name_id","tag":"nameId","tag_type":"select","label":"Personal Identifier","placehoder":"Name Identifier","help":"choose Identifier for name","additional_data":{"options":["ORICD","GND"]}}
];
	return{
		elements:elements
	}
})();

