// for a different languate copy this file an dchange the name acccordingly , then can change label, place holder, help and dropdown options in respective language.
// changing the tags and other fields are not recommended cause then you have to udpate html id's or the program which which process the metadata as well.

var dictionary_metadata_en=(function (){
	var indent={"id":1};
	var elements=[{"type":"field_input","id":"creator","tag":"creators","tag_type":"input","label":"Creator","placehoder":"Name as Input","validation_msg":"Required","help":"Name of the person who are responsible for the content"},
{"type":"add_button","id":"co_creators","tag":"co_creator","tag_type":"input","multiple_class":"creator"},
{"type":"dropdown_normal","id":"name_id","tag":"nameId","tag_type":"select","label":"Name Id","placehoder":"Name Identifier","help":"choose Identifier for name","additional_data":{"options":["ORICD","GND","ABC"]}},
{"type":"field_input","id":"title","tag":"title","tag_type":"input","label":"Title","placehoder":"Title of the content","validation_msg":"Required","help":"Title for the content"},
{"type":"field_input","id":"description","tag":"description","tag_type":"input","label":"Description","placehoder":"Description of the content","validation_msg":"Required","help":"Description for the content"},

{"type":"dropdown_normal","id":"institution","tag":"institution","tag_type":"select","label":"Institution","placehoder":"Select Institution","help":"choose Identifier for Institution",
"additional_data":{"options":["TUBS Braunschweig - Technische Universität",
"HSBS  Braunschweig - Hochschule für Bildende Künste",
"TUCZ  Clausthal-Zellerfeld - Technische Universität",
"JHEF  Elsfleth - Jade Hochschule",
"HSELE  Emden - Hochschule Emden/Leer",
"GAUG  Göttingen - Georg August Universität",
"HSG Göttingen - Hochschule für angewandte Wissenschaft und Kust",
"LUH  Hannover - Leibniz Universität",
"HSH  Hannover - Hochschule",
"MHH Hannover - Medizinische Hochschule",
"TIB  Hannover - Technische Informationsbibliothek (TIB)",
"THH  Hannover - Tierärztliche Hochschule",
"HSMTMH  Hannover - Hochschule für Musik, Theater und Medien",
"SUHI  Hildesheim - Stiftung Universität",
"AWKHI Hildesheim - Hochschule für angewandte Wissenschaft und Kust",
"NHRHI  Hildesheim - Norddeutsche Hochschule für Rechtspflege",
"AWKHO Holzminden - Hochschule für angewandte Wissenschaft und Kust",
"HSELL  Leer - Hochschule Emden/Leer",
"HSOSLI  Lingen - Hochschule Osnabrück",
"LULU  Lüneburg - Leuphana Universität",
"COUOL  Oldenburg - Carl von Ossietzky Universität",
"JHOL  Oldenburg - Jade Hochschule",
"UOS  Osnabrück - Universität Osnabrück",
"HSOS  Osnabrück - Hochschule Osnabrück",
"OHAWSZ  Salzgitter - Ostfalia Hochschule für angewandte Wissenschaften",
"OHAWSU  Suderburg - Ostfalia Hochschule für angewandte Wissenschaften",
"UVE  Vechta - Universität Vechta",
"JHWH Wilhelmshaven - Jade Hochschule",
"OHAWWF Wolfenbüttel - Ostfalia Hochschule für angewandte Wissenschaften",
"OHAWWO Wolfsburg - Ostfalia Hochschule für angewandte Wissenschaften"]}},

{"type":"field_input","id":"institution_id","tag":"institution_id","tag_type":"input","label":"Institution Id","placehoder":"Id of the institution","validation_msg":"Required","help":"Id of the institution"},
{"type":"field_input","id":"date","tag":"date","tag_type":"input","label":"Content Date","placehoder":"Date for the content","validation_msg":"Required","help":"Date for the content"},

{"type":"dropdown_normal","id":"language","tag":"language","tag_type":"select","label":"Language","placehoder":"Language of the Content","help":"Language of the Content",
"additional_data":{"options":[
"EN English",
"DE Deutsch",
"FR French",
"ES Spanish"
]}},

{"type":"dropdown_normal","id":"tom","tag":"tom","tag_type":"select","label":"Type of Material","placehoder":"Type of material","help":"what kind of material is it?",
"additional_data":{"options":[
"Other place of learning",
"Application",
"Worksheet",
"Working material",
"Audio",
"Task?",
"Image",
"Experiment",
"Interactive material",
"Map",
"Course",
"Assessment",
"Educational game",
"Index",
"Portal",
"Primary material/source",
"Projekt",
"Slide",
"Role play/business game",
"Schoolbook",
"Simulation",
"Skript",
"Text",
"Lesson plan",
"Video",
"Webquest",
"Tool",
"Exercise"
]}},

{"type":"dropdown_normal","id":"doi","tag":"doi","tag_type":"select","label":"DOI","placehoder":"DOI","help":"DOI",
"additional_data":{"options":[
"DOI",
"URN",
"Handle"
]}}

];
	return{
		elements:elements
	}
})();


