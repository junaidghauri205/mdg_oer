var metadataGenerator=(function (){
	var kernelVersion = "2.2";
	var kernelNamespace = "http://www.tib.eu/ext/knm/NTM-Metadata-Schema_v_2.2.xsd";
	var kernelSchema = "NTM-Metadata-Schema_v_2.2.xsd";
	var kernelSchemaLocation = kernelNamespace + " " + kernelSchema;
	var header = "";
	var dictElements=[];
	var MIME_TYPE = "application/xml";
	var outputfilename="metadata.xml"
	var indent={"tag":0,"multiple":1,"attributes":2}
	var xml ="";

	var init=function(){
		header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+br()+"<resource xmlns=\"" + kernelNamespace + "\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"" + kernelSchemaLocation + "\">";
		xml=header;
		dictElements=dictionary_metadata_en.elements;
		metadataGenerator.bindpageEvents();
		metadataGenerator.loadDictionaries();
		
	};
	var loadFieldInput=function(element){
		$("#"+element.id+"_label").text(element.label);
		$("#"+element.id+"_input").attr("placeholder",element.help);
	};
	var loadDropdown_normal=function(element){
		var options=element.additional_data.options;
		$("#"+element.id+"_label").text(element.label);
		for(x of options){
			$("#"+element.id+"_select").append(new Option(x, x))
		}
	};
	var loadDictionaries=function(){
		// for respective language Eng/Ger/other
		$(".validation_msg").text(dictElements[0].validation_msg)
		for(i = 0; i < dictElements.length; i++) { 
			if(dictElements[i].type==="field_input"){
				loadFieldInput(dictElements[i]);
			}
			if(dictElements[i].type==="dropdown_normal"){
				loadDropdown_normal(dictElements[i]);
			}
		}
	};
	var bindpageEvents=function(){
		$("#generate_button").off().on("click",function(e){
			metadataGenerator.generateXml();
			$("#out_area").val(xml);
		});
		$("#save_button").off().on("click",function(e){
			metadataGenerator.downloadFile();
		});
		$(".datepicker").datepicker();
	};
	

	var getFieldInputXml=function(element) {
		tagXml=ot(element.tag);
		tagXml+=$("#"+element.id+"_"+element.tag_type).val();
		tagXml+=ct(element.tag);
		tagXml+=br();
		return tagXml;
	};

	var generateXml=function() {
		//indent
		xml+=br()
		for(i = 0; i < dictElements.length; i++) { 
			if(dictElements[i].multiple===true){
				
			}
			else{
				//if(dictElements[i].type==="field_input"){} // here if attributes included then have to add loop for that
				if($("#"+dictElements[i].id+"_"+dictElements[i].tag_type).val()){
					xml+=getFieldInputXml(dictElements[i]);	
				}
			}	
		}
		xml+=ct("resource");
	};

	var cleanUp = function(a) {
	  setTimeout(function() {
	    window.URL.revokeObjectURL(a.href);
	  }, 1500);
	  $("span#output").html("");
	};
	var downloadFile = function() {
	  window.URL = window.webkitURL || window.URL;
	  var prevLink = $("span#output a");
	  if (prevLink) {
	    $("span#output").html("");
	  }
	  var bb = new Blob([xml], {type:MIME_TYPE});
	  var a = document.createElement("a");
	  a.download = outputfilename;
	  a.href = window.URL.createObjectURL(bb);
	  a.textContent = "Click here to Save: "+outputfilename;
	  a.setAttribute("data-downloadurl", [MIME_TYPE, a.download, a.href].join(":"));
	  a.classList.add("btn");
	  a.classList.add("btn-primary");
	  a.onclick = function(e) {
	    if ($(this).is(":disabled")) {
	      return false;
	    }

	    cleanUp(this);
	  };
	  $(a).appendTo($("span#output"));
	};
	
	var br=function() {
	  return "\n";
	};
	var tab=function(number){
		var tabs = "";
		if (typeof number !== "undefined"){
			for (var i = 1; i <= number; i++ ){
				tabs += "\t";
			}
		}
		else{
			tabs = "\t";
		}
		return tabs;
	};
	var ota=function(tag,attr) {
		if (attr.length > 0){
			return "<" + tag +" "+ attr + ">";
		}
		else{
			return ot(tag);
		}
	};
	var ot=function(tag) {
	  return "<" + tag + ">";
	};
	var ct=function(tag) {
		  return "</" + tag + ">";
	};

	return{
		init:init,
		downloadFile:downloadFile,
		loadDictionaries:loadDictionaries,
		generateXml:generateXml,
		bindpageEvents:bindpageEvents
	}
})();

$(document).ready(function(){
	metadataGenerator.init()
	});